const EventEmitter = require('events');

class Logger extends EventEmitter {
    log(message) {
        this.emit('logged', message);    
    }
}

module.exports = Logger;