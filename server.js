const http = require('http');

const server = http.createServer((req, res) => {

    if (req.url === '/') {
        res.write('hello !!!!');
        res.end();
    }

    if (req.url === '/users') {
        res.write(JSON.stringify([
            {
                id: 1,
                name: 'Mosh'
            },
            {
                id: 2,
                name: 'Madawa'
            }
        ]));
        res.end();
    }
    
});

server.listen(3000);

console.log('listning to port 3000 ...');